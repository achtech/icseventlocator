import urllib.request
import os
import glob
import icalendar
import datetime
import argparse


### ArgParser ###
parser = argparse.ArgumentParser(
                    prog='icsEventLocator',
                    description='Search for event occurrence by name',
                    epilog='Author : A.Chauvet, Licence : Apache 2.0')

parser.add_argument('ics_address', help='Web path to download .ics file to search in')
parser.add_argument('event_name', help='Events name that we want to locate')
parser.add_argument('--start_date', help='Date to start searching',default='1970-01-01')
parser.add_argument('--stop_date', help='Date to stop searching',default='2038-01-01')

args = parser.parse_args()
#################

### Global variables ###
address = args.ics_address
rawIcsFilePath = ".tmp/cal.ics"
cleanIcsFilePath = ".tmp/ccal.ics"
totalTime = datetime.timedelta()
nbEvent = 0
########################
print("icsEventLocator :")
if not os.path.exists(".tmp"):
    os.makedirs(".tmp")
urllib.request.urlretrieve(address, rawIcsFilePath)

if os.path.exists(rawIcsFilePath):
    print(".ics File successfully retrieved")

### Remove non standard lines ###
cf = open(cleanIcsFilePath,"w")
with open(rawIcsFilePath,"r") as f:
    for line in f:
        if line.find("P3600S") != -1:
            line = line.replace('P3600S','PT3600S')
        cf.write(line)
cf.close()
    #################################
with open(cleanIcsFilePath) as f:
    calendar = icalendar.Calendar.from_ical(f.read())
    print("Following events have been found :")
    for event in calendar.walk('VEVENT'):
        if event.get("SUMMARY") == args.event_name:
            stdt = event.decoded("DTSTART")
            stsearchdate = datetime.datetime.fromisoformat(args.start_date)
            if(stsearchdate.tzinfo == None):
                stsearchdate = stsearchdate.replace(tzinfo=datetime.timezone.utc)
            spsearchdate = datetime.datetime.fromisoformat(args.stop_date)
            if(spsearchdate.tzinfo == None):
                spsearchdate = spsearchdate.replace(tzinfo=datetime.timezone.utc)
                
            if(stdt >= stsearchdate and stdt <= spsearchdate):
                drdt = event.decoded("DURATION")
                print(f"Day : {stdt.strftime("%d/%m/%Y")}, Start time : {stdt.strftime("%H:%M")} Duration : {event.decoded("DURATION")}")
                totalTime += drdt
                nbEvent +=1
print(f"{nbEvent} events, Total duration = {totalTime} = {totalTime.total_seconds()/3600}h")
### Clean tmp folder ###
files = glob.glob('.tmp/*')
for f in files:
   os.remove(f)
########################